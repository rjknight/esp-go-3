/*
 * ssd1351.c
 *
 *  Basic ssd1351 access adapted from original segment here
 *  https://www.mgo-tec.com/blog-entry-adafruit-oled-ssd1351-esp-wroom-nonlib.html
 */

// In this SPI Driver:
// WEMOS D6 ESP8266 GPIO12 is MISO (din) - not used here
// WEMOS D7 ESP8266 GPIO13 is MOSI (dout)
// WEMOS D5 ESP8266 GPIO14 is clock
// WEMOS D8 ESP8266 GPIO15 is CS/SS = not used here - assume that CS tied low, reset is tied to Processor reset
// WEMOS D0 ESP8266 GPIO16 as the D/C line.
// 5v power and ground

// assumptions - only SPI device so no CS...  after any command need DC_HI then spi data - so do that in command...

#include "aidan_and_petes.h"
#include "easygpio/easygpio.h"
#include <math.h>
#include "spi.h"
#include "glcd_fonts.h"


//#define CS_HI easygpio_outputSet(15,1);
//#define CS_LO easygpio_outputSet(15,0);
//#define RST_HI
//#define RST_LO
#define DC_HI easygpio_outputSet(16,1);
#define DC_LO easygpio_outputSet(16,0);

 static uint8_t x1351=0;
 static uint8_t x1351_master=0;
 static uint8_t y1351=0;

 static uint16_t RGBfore=0xffff;
 static uint16_t RGBback=0;

 static const uint8 *ch32;

 static uint8_t _height;
 static uint8_t _width;
 static uint8_t _offset;
 static uint8_t bytes_high;
 static uint32_t c_size;
 static uint8_t currentFont=9;
 static uint8_t mono=0;

 static uint32_t thevalue=0;

 void IFA ssd1351Dot(uint8_t x, uint8_t y);

#define swap(a,b) {a^=b; b^=a; a^=b;}

 void IFA writeCommand(uint8_t c) {
 	DC_LO;
// 	CS_LO;
   spi_tx8(1, c);
//    CS_HI;
   DC_HI
 }


 void IFA writeData(uint8_t c) {
 	DC_HI;
// 	CS_LO;
 	spi_tx8(1, c);
// 	CS_HI;
 }


 void IFA ssd1351SetAddrWindow(uint8_t x0, uint8_t y0, uint8_t x1,uint8_t y1) {
    writeCommand(0x15); // Column addr set
    spi_tx16(1,x1+(x0<<8));
    writeCommand(0x75); // Row addr set
    spi_tx16(1,y1+(y0<<8));
    writeCommand(0x5c); // write to RAM - do this here - may as well as can't READ from these devices
  }




 void IFA ssd1351Clear(uint8_t x1,uint8_t y1,uint16_t x2,uint16_t y2){
	 uint16_t tot;
	 tot=((x2-x1)+1)*((y2-y1)+1);
	 ssd1351SetAddrWindow(x1,y1,x2,y2);
	 while (tot--) spi_tx16(1,RGBback);
	 //for(int i=0; i<tot; i++){
	 //    spi_tx16(1,RGBback);
	 // }
 }

 void IFA ssd1351DrawHLine(uint8_t x0,uint8_t y0,uint16_t w){
	  ssd1351SetAddrWindow(x0,y0,x0+w-1,y0);
      while (w--) spi_tx16(1,RGBfore);
//	  CS_LO
//	  for(int i=0; i<w; i++){
//	    spi_tx16(1,RGBfore);
// 	  }
//      CS_HI
  }

 void IFA ssd1351DrawVLine(uint8_t x0,uint8_t y0,uint16_t h) {
	  ssd1351SetAddrWindow(x0,y0,x0,y0+h-1);
      while (h--) spi_tx16(1,RGBfore);
//	  CS_LO
// 	  for(int i=0; i<h; i++){
//	    spi_tx16(1,RGBfore);
// 	  }
//      CS_HI
  }



 void IFA ssd1351DrawLine(int16_t x0, int16_t y0, int16_t x1, int16_t y1) {

  	if (y0 == y1) {
  		if (x1 > x0) {
  			ssd1351DrawHLine(x0, y0, x1-x0+1);
  		} else if (x1 < x0) {
  			ssd1351DrawHLine(x1, y0, x0-x1+1);
  		} else {
  			ssd1351Dot(x0, y0);
  		}
  		return;
  	} else if (x0 == x1) {
  		if (y1 > y0) {
  			ssd1351DrawVLine(x0, y0, y1-y0+1);
  		} else {
  			ssd1351DrawVLine(x0, y1, y0-y1+1);
  		}
  		return;
  	}

  	uint8_t steep = abs(y1 - y0) > abs(x1 - x0);
  	if (steep) {
  		swap(x0, y0);
  		swap(x1, y1);
  	}
  	if (x0 > x1) {
  		swap(x0, x1);
  		swap(y0, y1);
  	}

  	int16_t dx, dy;
  	dx = x1 - x0;
  	dy = abs(y1 - y0);

  	int16_t err = dx / 2;
  	int16_t ystep;

  	if (y0 < y1) {
  		ystep = 1;
  	} else {
  		ystep = -1;
  	}

  	int16_t xbegin = x0;
  	if (steep) {
  		for (; x0<=x1; x0++) {
  			err -= dy;
  			if (err < 0) {
  				int16_t len = x0 - xbegin;
  				if (len) {
  						ssd1351DrawVLine(y0, xbegin, len + 1);
  				} else {
  						ssd1351Dot(y0, x0);
  				}
  				xbegin = x0 + 1;
  				y0 += ystep;
  				err += dx;
  			}
  		}
  		if (x0 > xbegin + 1) ssd1351DrawVLine(y0, xbegin, x0 - xbegin);
  	} else {
  		for (; x0<=x1; x0++) {
  			err -= dy;
  			if (err < 0) {
  				int16_t len = x0 - xbegin;
  				if (len) {
  						ssd1351DrawHLine(xbegin, y0, len + 1);
  				} else {
  						ssd1351Dot(x0, y0);
  				}
  				xbegin = x0 + 1;
  				y0 += ystep;
  				err += dx;
  			}
  		}
  		if (x0 > xbegin + 1) ssd1351DrawHLine(xbegin, y0, x0 - xbegin);
  	}
  }



 void IFA ssd1351Dot(uint8_t x, uint8_t y){
   ssd1351SetAddrWindow(x,y,x,y);
   spi_tx16(1,RGBfore);
 }


// nicked the idea of a table for startup parameters from elsewhere...  Saves lots of code.
static const uint8_t IRA ssd1351InitVars[] = {
	   25,
	   0xfd,1,0x12,
	   0xfd,1,0x12, //Set Command Lock - Unlock OLED driver IC MCU interface from entering command
	   0xfd,1,0xb1, //Set Command Lock - Command A2,B1,B3,BB,BE,C1 accessible if in unlock state
	   0xae,0, //Sleep mode On (Display OFF)
	   0xb3,0, //Front Clock Divider
	   0xf1,0, // 7:4 = Oscillator Frequency, 3:0 = CLK Div Ratio (A[3:0]+1 = 1..16)
	   0xca,1,127, //Set MUX Ratio
	   0xa0,1,0x75, //Set Re-map 65k color  - 0x74 for horizontal scan - I've set to vertical
	   0x15,2,0,127, // Set column start and end
	   0x75,2,0,127, // set row start and end
	   0xa1,1,0,  // set display start line
	   0xa2,1,0,  // set display offset
	   0xb5,1,0,  // set gpio
	   0xab,1,1, // Function Selection - Enable internal Vdd /8-bit parallel
	   0xb1,0, // Set Reset(Phase 1) /Pre-charge(Phase 2)
	   0x74,0,  // Set VCOMH Voltage
	   0xbe,0, // Set VCOMH Voltage
	   0x05,0, // 0.82 x VCC [reset]
	   0xa6,0, // Reset to normal display
	   0xc1,3,0xc8,0x80,0xc8, // Set Contrast - Red contrast (reset=0x8A) Green contrast (reset=0x51) Blue contrast (reset=0x8A)
	   0xc7,1,0x0f, // Master Contrast Current Control 0-15
	   0xb4,3,0xa0,0xb5,0x55, // Set Segment Low Voltage(VSL)
	   0xb6,1,1, // Set Second Precharge Period
	   0x9e,0, // Scroll Stop Moving
	   0xaf,0, // Sleep mode On (Display ON)
	   };   // end


void IFA ssd1351Init(void) {
	   const uint8_t *ssdp;
	   uint8_t nxt;
	   uint8_t numc;
	   uint8_t numa;
	   uint8_t isdelay;
	   ssdp=ssd1351InitVars;

	   numc=read_rom_uint8(ssdp++);
	   while (numc--)
   	   {
	   writeCommand(read_rom_uint8(ssdp++));
	   numa=read_rom_uint8(ssdp++);
	   isdelay=numa&128;
	   numa&=127;
	   while (numa--) writeData(read_rom_uint8(ssdp++));
	   if (isdelay) os_delay_us(100000);
   	   }
 }



// draw a rectangle
void IFA ssd1351DrawRect(uint16_t x, uint16_t y, uint16_t w, uint16_t h) {
	 ssd1351DrawVLine(x,y,h);
	 ssd1351DrawVLine(x+(w-1),y,h);
	 ssd1351DrawHLine(x+1,y,w-2);
	 ssd1351DrawHLine(x+1,y+(h-1),w-2);
}

// fill a rectangle
void IFA ssd1351FillRect(uint16_t x, uint16_t y, uint16_t w, uint16_t h) {
	 uint32_t tot;
	 ssd1351DrawRect(x,y,w,h);
	 ssd1351SetAddrWindow(x+1, y+1, x+w-2, y+h-2);
	 tot=(h-2)*(w-2);
    while (tot--) spi_tx16(1,RGBback);
}


 void IFA ssd1351Fore(uint8_t r, uint8_t g, uint8_t b)
 {
	 RGBfore=((r&0xf8)<<8)|((g&0xfc)<<3)|(b>>3);
 }
 void IFA ssd1351Back(uint8_t r, uint8_t g, uint8_t b)
 {
	 RGBback=((r&0xf8)<<8)|((g&0xfc)<<3)|(b>>3);
 }

 void IFA ssd1351Cursor(uint8_t x, uint8_t y)
 {
	 x1351=x; y1351=y;
 }

 uint8_t IFA ssd1351GCLDChar(uint8_t ch){
        uint8_t cwid;
 	    uint8_t i;
 	    uint8_t var_width;
        uint8_t t_height;
 		uint16_t x;
 		uint16_t y;
 		const uint8 *lch32;

 		lch32=ch32;
        x=x1351; y=y1351;
 	    lch32+=((ch-_offset)*c_size);
 	    cwid=read_rom_uint8(lch32++);
 	    if (mono) var_width=_width; else var_width=cwid; // this should speed up non-mono output - less columns

        // high speed char drawing
 	   ssd1351SetAddrWindow(x,y,x+var_width-1,y+_height-1);
   	    for ( i = 0; i < var_width; i++ ) {
                       uint32_t j;
                       uint16_t xi=x+i;
                       const uint8 *ib=lch32+(i*bytes_high);
                       t_height=_height; // only do ACTUALLY required height
                       for ( j = 0; j < bytes_high; j++ ) {
                       	   	   uint16_t jy=(j<<3)+y;
                               uint8_t dat = read_rom_uint8( ib + j );
                               uint8_t bit;
                               for (bit = 0; bit < 8; bit++) {
                                      if (t_height)
										 {
                                          spi_tx16(1,(dat & (1<<bit)) ? RGBfore : RGBback   );
										  --t_height;
										 }
                               }
                       }
               }


  x1351+=var_width;  return cwid;
 }


static IFA char *parse( char *p)
 		{
 	    thevalue=0;
 	    while ((*p>='0')&&(*p<='9')) { thevalue*=10; thevalue+=(*p)-'0'; p++; }
 		if (*p==';') return ++p;
 		if (*p==',') return ++p;
 		return p;
 		}


static void IFA ssd1351GCLDFont(uint8_t font)
 {
	currentFont=font;  // save for switching elsewhere
	switch (font)
	{
	case 1 : ch32=digital36x69; break;
	case 2 : ch32=digital17x32; break;
	case 3 : ch32=trebuchet; break;
	case 4 : ch32=calibri21x22; break;
	case 5 : ch32=calibri32x34; break;
	case 6 : ch32=petesfont24x28; break;
	case 7 : ch32=meteocons21x25; break;
	case 8 : ch32=ms_reference_sans_serif16x16; break;
	case 9 : ch32=Basic8x8; break;
	case 10: ch32=assorted1_19x21; break;
	case 11: ch32=assorted2_19x21; break;
	case 12: ch32=assorted3_19x21; break;
	case 0 :
	default: ch32=calibri16x17; break;
	}
    _width=read_rom_uint8(ch32++);
    _height=read_rom_uint8(ch32++);
    _offset=read_rom_uint8(ch32++);
	bytes_high=(_height+7)>>3; // rounded up bytes for a vertical slice of the char
    c_size=((_width*bytes_high)+1);
 }


void IFA ssd1351glyph(uint8_t wf, uint8_t x)
{
	 uint8_t tfont;
	 tfont=currentFont;
	 switch (wf)
	 {
	 case 1 :ssd1351GCLDFont(10); break;
	 case 2 :ssd1351GCLDFont(11); break;
	 case 3 :ssd1351GCLDFont(12); break;
	 default :ssd1351GCLDFont(10); break;
	 }
	 ssd1351GCLDChar(x);
	 ssd1351GCLDFont(tfont);
}


 void IFA ssd1351GCLDString(uint8_t* st){
	 char tstore[12];
	 while (*st)
 	 {
     if (*st=='\n')  { x1351=x1351_master; y1351+=_height; st++; continue;  }
	 if (*st=='\r')  { st++; continue;  }
	 switch (*st)
	 	 {

	 	 case '$' : switch (*++st)
						 {
	 	 	 	 	 	 case '$' : break;
	 	 	 	 	 	 //case 'l' : ssd1351Clear(0,0,127,127); ++st;
			             //           continue;

	 	 	 	 	 	 case 'S' :
	 	 	 	 	 	 	 	 sysCfg.enable13 = 1; // stop 13 use as an indicator
	 	 	 	 	 	 	 	 spiActive = 1;
	 	 	 	 	 	 	 	 easygpio_pinMode(16, EASYGPIO_NOPULL, EASYGPIO_OUTPUT);
	 	 	 	 	 	 	 	 spi_init(1); // HSPI mode
	 	 	 	 	 	 	 	 spi_clock(1,2,2); // will not work without this - too fast for display
	 	 	 	 	 	 	 	 ssd1351Init();
	 	 			             os_delay_us(100000);
	 	 	 	 	 	 	 	 ssd1351GCLDFont(9);
	 			                 ssd1351Fore(255,255,255);
	 	 	 			         ssd1351Back(0,0,0);
	 	 	 			         ssd1351Clear(0,0,127,127);
	 	 	 			         ssd1351Cursor(0,0);
	 	 			             ++st;
	 	 			     	     spiActive=0;
	 	 			             continue;

	 	 	 	 	 	 case 'P' : st=parse(++st); int xx=thevalue; st=parse(st); int yy=thevalue; x1351_master=xx; x1351=xx; y1351=yy; continue;
	 	 	 	 	 	 case 'C' : st=parse(++st); int rr=thevalue; st=parse(st); int gg=thevalue; st=parse(st); int bb=thevalue; ssd1351Fore(rr,gg,bb); continue;
	 	 	 	 	 	 case 'B' : st=parse(++st); rr=thevalue; st=parse(st); gg=thevalue; st=parse(st); bb=thevalue; ssd1351Back(rr,gg,bb); continue;
	 	 	 	 	 	 case 'M' : st=parse(++st); if (thevalue==1) mono=1; else mono=0; continue;
	 	 	 	 	     case 'F' : st=parse(++st); if (thevalue<=12) ssd1351GCLDFont(thevalue); continue;

	 	 	 	 	     case 'L' : st=parse(++st); xx=thevalue; st=parse(st); yy=thevalue; st=parse(st); int xx2=thevalue; st=parse(st); int yy2=thevalue;
	 	 	 	 	     	 	 	ssd1351DrawLine(xx,yy,xx2,yy2);continue;
	 		 	 	     case 'E' : st=parse(++st); xx=thevalue; st=parse(st); yy=thevalue; st=parse(st); xx2=thevalue; st=parse(st); yy2=thevalue;
	 		 	 	     	 	 	 	 	 ssd1351DrawRect(xx,yy,xx2,yy2);continue;	 // 2nd param width and height
	 		 	 	 	 case 'I' : st=parse(++st); xx=thevalue; st=parse(st); yy=thevalue; st=parse(st); xx2=thevalue; st=parse(st); yy2=thevalue;
	 		 	 	 	 	     	 	 	ssd1351FillRect(xx,yy,xx2,yy2); continue;	 // 2nd param width and height
	 		 	 	 	 case 'W' : ssd1351FillRect(0,0, 127,127); ++st; continue;

	 	 	 	 	 	 case 'T' : ++st; os_sprintf(tstore,"%02d:%02d:%02d",tm.Hour, tm.Minute, tm.Second); ssd1351GCLDString(tstore); continue;
	 	 	 	 	 	 case 'D' : ++st; os_sprintf(tstore,"%02d:%02d:%02d",tm.Day, tm.Month, tm.Year); ssd1351GCLDString(tstore); continue;
	 	 	 	 	 	 case 'A' : ++st; struct ip_info theIp; wifi_get_ip_info(0, &theIp);
	 	 	 	 	 				os_sprintf(tstore, "%d:%d:%d:%d", theIp.ip.addr & 255, (theIp.ip.addr >> 8) & 255, (theIp.ip.addr >> 16) & 255, theIp.ip.addr >> 24);
	 	 	 	 	 			    ssd1351GCLDString(tstore); continue;
	 	 	 	 	     case 'G' : st=parse(++st); xx=thevalue; if ((xx<1) || (xx>3)) continue;
	 	 	 	 	 	 	 	 	if (*st==0) continue;
	 	 	 	 	 	 	 	 	ssd1351glyph(xx,*st++); continue;
						 }
	 	 default:  break;
	 	 }
	 ssd1351GCLDChar(*st++);
	 }
 }


