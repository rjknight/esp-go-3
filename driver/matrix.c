
#include <stdlib.h>
#include "osapi.h"
#include "debug.h"
#include "easygpio/easygpio.h"
#include "aidan_and_petes.h"

static uint8_t _dataPin;
static uint8_t _clockPin;
static uint8_t _intensity;
static uint8_t disBuffer[8];

void IFA MLED_init(uint8_t intensity, uint8_t dataPin, uint8_t clockPin)
{
	_dataPin = dataPin;
	_clockPin = clockPin;

	if(intensity>7)
		_intensity=7;
	else
		_intensity=intensity;

	easygpio_outputSet(dataPin, 1);
	easygpio_outputSet(clockPin, 1);

}

void IFA MLED_clear()
{
	for(uint8_t i=0;i<8;i++)
	{
		disBuffer[i]=0x00;
	}
}

void IFA MLED_dot(uint8_t x, uint8_t y, uint8_t draw)
{
	x&=0x07;
	y&=0x07;

	if(draw)
	{
		disBuffer[y]|=(1<<x);
	}
	else
	{
		disBuffer[y]&=~(1<<x);
	}

}

void IFA MLED_send(uint8_t data)
{
  for (int i = 0; i < 8; i++) {
    easygpio_outputSet(_clockPin, 0);
    easygpio_outputSet(_dataPin, data & 1 ? 1 : 0);
    data >>= 1;
    easygpio_outputSet(_clockPin, 1);
  }
}

void IFA MLED_sendCommand(uint8_t cmd)
{
  easygpio_outputSet(_dataPin, 0);
  MLED_send(cmd);
  easygpio_outputSet(_dataPin, 1);
}

void IFA MLED_sendData(uint8_t address, uint8_t data)
{
  MLED_sendCommand(0x44);
  easygpio_outputSet(_dataPin, 0);
  MLED_send(0xC0 | address);
  MLED_send(data);
  easygpio_outputSet(_dataPin, 1);
}

void IFA MLED_display()
{

	for(uint8_t i=0;i<8;i++)
	{
	MLED_sendData(i,disBuffer[i]);

		easygpio_outputSet(_dataPin, 0);
  		easygpio_outputSet(_clockPin, 0);
  		easygpio_outputSet(_clockPin, 0);
  		easygpio_outputSet(_dataPin, 0);
	}
	MLED_sendCommand(0x88|(_intensity));
}
