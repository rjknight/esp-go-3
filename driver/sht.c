// INA219 is current and voltage sensor - this code is mainly derived from Adafruit
// https://github.com/adafruit/Adafruit_INA219

#include <stdlib.h>
#include "osapi.h"
#include "i2c.h"
#include "ssd1306.h"
#include "debug.h"
#include "font_mod.h"
#include "glcd_fonts.h"
#include "wire86.h"
#include "aidan_and_petes.h"

static uint8_t sht30_started=0;
/*=========================================================================
    I2C ADDRESS/BITS
    -----------------------------------------------------------------------*/
  #define SHT_30_ADDRESS                         (0x45)



  void IFA wireWrite_sht30()
  {
	  uint8_t buf[2];
	  buf[0]=0x24;
	  buf[1]=0x0;
	  twi_writeTo(SHT_30_ADDRESS,buf,2,1);
  }

  void IFA sht30_begin() {
            twi_init();  /// experiment using variation of WIRE source code
            wireWrite_sht30();
            os_delay_us(5000000);
  }

  void IFA wireRead_sht30(int *tp, int *hum, int *htp)
  {
	  uint8_t buf[6];
	  if (sht30_started==0) { sht30_begin(); sht30_started=1; }
      twi_readFrom(SHT_30_ADDRESS,buf,6,1);
      wireWrite_sht30();

      *tp=(((buf[0]*256)+buf[1])*175/65535)-45;

      *htp=(((buf[0]*256)+buf[1])*1750/65535)-450;

      *hum=(((buf[3]*256)+buf[4])*100/65535);

    //  iprintf(RESPONSE, "Temp %d\r\n", (((buf[0]*256)+buf[1])*175/65535)-45);
    //  iprintf(RESPONSE, "Hum %d\r\n", (((buf[3]*256)+buf[4])*100/65535));
  }

