/*

SSD1306 OLED I2C driver

Derk Steggewentz, 3/2015 

This is a I2C driver for SSD1306 OLED displays including graphics library.


The graphics library part (gfx_ functions) is based on the Adafruit graphics library.


No license restrictions on my part, do whatever you want as long as you follow the 
inherited license requirements (applying to the Adafruit graphics library part).


===== derived license (for graphics library) ======

Copyright (c) 2013 Adafruit Industries. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
- Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
- Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*/

#include <stdlib.h>
#include "osapi.h"
#include "i2c.h"
#include "ssd1306.h"
#include "debug.h"
#include "font_mod.h"
#include "glcd_fonts.h"
#include "aidan_and_petes.h"
#include "wire86.h"

#define DISPLAYWIDTH 128
#define DISPLAYHEIGHT 64
    
#define SSD1306_SETCONTRAST 0x81
#define SSD1306_DISPLAYALLON_RESUME 0xA4
#define SSD1306_DISPLAYALLON 0xA5
#define SSD1306_NORMALDISPLAY 0xA6
#define SSD1306_INVERTDISPLAY 0xA7
#define SSD1306_DISPLAYOFF 0xAE
#define SSD1306_DISPLAYON 0xAF
#define SSD1306_SETDISPLAYOFFSET 0xD3
#define SSD1306_SETCOMPINS 0xDA
#define SSD1306_SETVCOMDETECT 0xDB
#define SSD1306_SETDISPLAYCLOCKDIV 0xD5
#define SSD1306_SETPRECHARGE 0xD9
#define SSD1306_SETMULTIPLEX 0xA8
#define SSD1306_SETLOWCOLUMN 0x00
#define SSD1306_SETHIGHCOLUMN 0x10
#define SSD1306_SETSTARTLINE 0x40
#define SSD1306_MEMORYMODE 0x20
#define SSD1306_COLUMNADDR 0x21
#define SSD1306_PAGEADDR 0x22
#define SSD1306_COMSCANINC 0xC0
#define SSD1306_COMSCANDEC 0xC8
#define SSD1306_SEGREMAP 0xA0
#define SSD1306_CHARGEPUMP 0x8D
#define SSD1306_EXTERNALVCC 0x1
#define SSD1306_SWITCHCAPVCC 0x2
#define SSD1306_ACTIVATE_SCROLL 0x2F
#define SSD1306_DEACTIVATE_SCROLL 0x2E

static uint8_t mono=0;

static uint8_t ssd1306_started=0;
static uint8_t ssd1306_used=0;

static uint8_t screen_width=128;
static uint8_t screen_height=64;

static uint8_t _offset;
static uint8_t bytes_high;
static const uint8_t *ch32;
static uint32_t c_size;

static uint16_t x_master=0;

// I2C result status
#define TRANSFER_CMPLT    (0x00u)
#define TRANSFER_ERROR    (0xFFu)

static void display_write_buf( uint8* buf, uint16_t size );

// void gfx_init( int16_t width, int16_t height );

static uint8 oled_addr=0x3c; // THIS NEEDS TO BE PROGRAMMABLE FOR THE DISPLAYS COMING FROM CHINA.....

static uint8_t currentFont=0;

// display memory buffer ( === MUST INCLUDE === the preceding I2C 0x40 control byte for the display)
static uint8_t SSD1306_buffer[DISPLAYHEIGHT * DISPLAYWIDTH / 8 + 1] = { 0x40 };
// pointer to actual display memory buffer
static uint8_t* _displaybuf = SSD1306_buffer+1;
static uint16_t _displaybuf_size = sizeof(SSD1306_buffer) - 1;

// see data sheet page 25 for Graphic Display Data RAM organization
// 8 pages, each page a row of DISPLAYWIDTH bytes
// start address of of row: y/8*DISPLAYWIDTH
// x pos in row: == x 
#define GDDRAM_ADDRESS(X,Y) ((_displaybuf)+((Y)/8)*(DISPLAYWIDTH)+(X))

// lower 3 bit of y determine vertical pixel position (pos 0...7) in GDDRAM byte
// (y&0x07) == position of pixel on row (page). LSB is top, MSB bottom
#define GDDRAM_PIXMASK(Y) (1 << ((Y)&0x07))

#define PIXEL_ON(X,Y) (*GDDRAM_ADDRESS(x,y) |= GDDRAM_PIXMASK(y))
#define PIXEL_OFF(X,Y) (*GDDRAM_ADDRESS(x,y) &= ~GDDRAM_PIXMASK(y))
#define PIXEL_TOGGLE(X,Y) (*GDDRAM_ADDRESS(x,y) ^= GDDRAM_PIXMASK(y)) 

// call before first use of other functions
void IFA display_init_32( void ){
	screen_height=32;
	gfx_setCursor( 10, 10 );
    
    uint8 cmdbuf[] = {
        0x00,
        SSD1306_DISPLAYOFF,
        SSD1306_SETDISPLAYCLOCKDIV,
        0x80,
        SSD1306_SETMULTIPLEX,
        0x1f,    // please note - 3f for the 64 line displays - 1f for the 32 line displays
        SSD1306_SETDISPLAYOFFSET,
        0x00,
        SSD1306_SETSTARTLINE | 0x0,
        SSD1306_CHARGEPUMP,
        0x14,
        SSD1306_MEMORYMODE,
        0x00,
        SSD1306_SEGREMAP | 0x1,
        SSD1306_COMSCANDEC,
        SSD1306_SETCOMPINS,
        0x2,  // use 0x12 for the 64 line display
        SSD1306_SETCONTRAST,
        0xcf,
        SSD1306_SETPRECHARGE,
        0xf1,
        SSD1306_SETVCOMDETECT,
        0x40,
        SSD1306_DISPLAYALLON_RESUME,
        SSD1306_NORMALDISPLAY,
		SSD1306_DISPLAYON
    };
    
    display_write_buf( cmdbuf, sizeof(cmdbuf) );
}

// call before first use of other functions
void IFA display_init_64( void ){
	screen_height=64;
	gfx_setCursor( 10, 10 );

    uint8 cmdbuf[] = {
        0x00,
        SSD1306_DISPLAYOFF,
        SSD1306_SETDISPLAYCLOCKDIV,
        0x80,
        SSD1306_SETMULTIPLEX,
        0x3f,    // please note - 3f for the 64 line displays - 1f for the 32 line displays
        SSD1306_SETDISPLAYOFFSET,
        0x00,
        SSD1306_SETSTARTLINE | 0x0,
        SSD1306_CHARGEPUMP,
        0x14,
        SSD1306_MEMORYMODE,
        0x00,
        SSD1306_SEGREMAP | 0x1,
        SSD1306_COMSCANDEC,
        SSD1306_SETCOMPINS,
        0x12,  // use 0x12 for the 64 line display
        SSD1306_SETCONTRAST,
        0xcf,
        SSD1306_SETPRECHARGE,
        0xf1,
        SSD1306_SETVCOMDETECT,
        0x40,
        SSD1306_DISPLAYALLON_RESUME,
        SSD1306_NORMALDISPLAY,
		SSD1306_DISPLAYON
    };

    display_write_buf( cmdbuf, sizeof(cmdbuf) );
}



// useful to turn off display if not used by application w/o going through the init process
void IFA display_off(void){
//    _i2caddr = i2caddr;
//    _i2caddr = oled_addr;
	uint8 cmdbuf[] = {
        0x00,
		SSD1306_DISPLAYOFF
	};
	display_write_buf( cmdbuf, sizeof(cmdbuf) ); 
    //SSD1306_command(SSD1306_DISPLAYOFF); // 0xAE    
}
void IFA display_on(void)
{
//  sendcommand(0xaf);        //display on
//  _i2caddr = oled_addr;
	uint8 cmdbuf[] = {
      0x00,
      SSD1306_DISPLAYON
	};
	display_write_buf( cmdbuf, sizeof(cmdbuf) );
}

/*
// for submitting command sequences:
//  buf[0] must be 0x00
// for submitting bulk data (writing to display RAM):
//  buf[0] must be 0x40
static uint32 IFA display_write_buf( uint8* buf, uint16_t size ){
    uint8 ack;
    i2c_master_start();
    i2c_master_writeByte(oled_addr<<1);
    ack = i2c_master_getAck();

    if (ack) {
        iprintf(INFO,"i2c address not acknowledged, display_write_buf\r\n" );
        i2c_master_stop();
        return TRANSFER_ERROR;
    }
 
 	//uint16_t txlen = sizeof(_buffer);
	uint16_t i = 0;
	
	for( i = 0 ; i < size ; i++ ){
		i2c_master_writeByte( buf[i] );
		ack = i2c_master_getAck();
		if (ack) {
		    iprintf(INFO,"i2c data byte not acknowledged, display_write_buf\r\n" );
			i2c_master_stop();
			return TRANSFER_ERROR;
		}
	}
    i2c_master_stop();
    return TRANSFER_CMPLT;
}


*/

// for submitting command sequences:
//  buf[0] must be 0x00
// for submitting bulk data (writing to display RAM):
//  buf[0] must be 0x40
static void IFA display_write_buf( uint8* buf, uint16_t size){
    if (ssd1306_started==0) { ssd1306_started=1; twi_init(); }
    twi_writeTo(oled_addr,buf,size,1);
}


// used by gfx_ functions. Needs to be implemented by display_
static void  IFA ssd1306SetPixel( int16_t x, int16_t y, uint16_t color ){
    
    if( (x < 0) || (x >= screen_width) || (y < 0) || (y >= screen_height) )
        return;

    switch( color ){
        case WHITE: 
            PIXEL_ON(x,y);
            break;
        case BLACK:
            PIXEL_OFF(x,y);
            break;
        case INVERSE: 
            PIXEL_TOGGLE(x,y);
            break;
    }
}
void IFA display_clear(void){
    os_memset( _displaybuf, 0x00, _displaybuf_size );
    SSD1306_buffer[0] = 0x40; // to be sure its there
}
// contrast: 0 ...255
void IFA display_contrast( uint8_t contrast ){
        
    uint8 cmdbuf[] = {
        0x00,  
        SSD1306_SETCONTRAST,
        contrast
    };
    display_write_buf( cmdbuf, sizeof(cmdbuf) ); 
}
// invert <> 0 for inverse display, invert == 0 for normal display
void IFA display_invert( uint8_t invert ){

    uint8 cmdbuf[] = {
        0x00,  
        0
    };
    cmdbuf[1] = invert ? SSD1306_INVERTDISPLAY : SSD1306_NORMALDISPLAY;
    display_write_buf( cmdbuf, sizeof(cmdbuf) ); 
}
void IFA display_update(void) {
      
    uint8 cmdbuf[] = {
        0x00,
        SSD1306_COLUMNADDR,
        0,                      // start
        DISPLAYWIDTH-1, // end
        SSD1306_PAGEADDR,
        0,                      // start
        7                       // end
    };
    display_write_buf( cmdbuf, sizeof(cmdbuf) ); 
    display_write_buf( SSD1306_buffer, sizeof(SSD1306_buffer) );
}
// draws horizontal or vertical line
// Note: no check for valid coords, this needs to be done by caller
// should only be called from gfx_hvline which is doing all validity checking
static void IFA display_line( int16_t x1, int16_t y1, int16_t x2, int16_t y2, uint16_t color ){

    if( x1 == x2 ){
        // vertical
        uint8_t* pstart = GDDRAM_ADDRESS(x1,y1);
        uint8_t* pend = GDDRAM_ADDRESS(x2,y2);       
        uint8_t* ptr = pstart;             
        
        while( ptr <= pend ){
            
            uint8_t mask;
            if( ptr == pstart ){
                // top
                uint8_t lbit = y1 % 8;
                // bottom (line can be very short, all inside this one byte)
                uint8_t ubit = lbit + y2 - y1;
                if( ubit >= 7 )
                    ubit = 7;
                mask = ((1 << (ubit-lbit+1)) - 1) << lbit;    
            }else if( ptr == pend ){
                // top is always bit 0, that makes it easy
                // bottom
                mask = (1 << (y2 % 8)) - 1;    
            }

            if( ptr == pstart || ptr == pend ){
                switch( color ){
                    case WHITE:     *ptr |= mask; break;
                    case BLACK:     *ptr &= ~mask; break;
                    case INVERSE:   *ptr ^= mask; break;
                };  
            }else{
                switch( color ){
                    case WHITE:     *ptr = 0xff; break;
                    case BLACK:     *ptr = 0x00; break;
                    case INVERSE:   *ptr ^= 0xff; break;
                };  
            }
            
            ptr += DISPLAYWIDTH;
        }
    }else{
        // horizontal
        uint8_t* pstart = GDDRAM_ADDRESS(x1,y1);
        uint8_t* pend = pstart + x2 - x1;
        uint8_t pixmask = GDDRAM_PIXMASK(y1);    

        uint8_t* ptr = pstart;
        while( ptr <= pend ){
            switch( color ){
                case WHITE:     *ptr |= pixmask; break;
                case BLACK:     *ptr &= ~pixmask; break;
                case INVERSE:   *ptr ^= pixmask; break;
            };
            ptr++;
        }
    }
}


// ============================================================
// graphics library stuff

int16_t WIDTH, HEIGHT; // This is the 'raw' display w/h - never changes
static int16_t _width, _height; // Display w/h as modified by current rotation
static int16_t cursor_x, cursor_y;
uint8_t wrap; // If set, 'wrap' text at right edge of display


void IFA gfx_setCursor( int16_t x, int16_t y ){
	x_master = x;
    cursor_x = x;
    cursor_y = y;
}


// helper function for gfx_drawLine, handles special cases of horizontal and vertical lines
static void IFA gfx_hvLine( int16_t x1, int16_t y1, int16_t x2, int16_t y2, uint16_t color ){
    
    if( x1 != x2 && y1 != y2 ){
        // neither vertical nor horizontal
        return;
    }    
    
    // bounds check

        if( x1 < 0 || x1 >= screen_width || x2 < 0 || x2 >= screen_width )
            return;
        if( y1 < 0 || y1 >= screen_height || y2 < 0 || y2 >= screen_height )
            return;


    // ensure coords are from left to right and top to bottom
    if( (x1 == x2 && y2 < y1) || (y1 == y2 && x2 < x1) ){
        // swap as needed
        int16_t t = x1; x1 = x2; x2 = t;
        t = y1; y1 = y2; y2 = t;
    }
    display_line( x1, y1, x2, y2, color );
}

// always use this function for line drawing
void IFA gfx_drawLine( int16_t x0, int16_t y0, int16_t x1, int16_t y1, uint16_t color ){
 
    if((x0 == x1) || (y0 == y1) ){
        // vertical and horizontal lines can be drawn faster
        gfx_hvLine( x0, y0, x1, y1, color );
        return;
    }
    
    int16_t t;
    
    int16_t steep = abs(y1 - y0) > abs(x1 - x0);
    if( steep ){
        t = x0; x0 = y0; y0 = t;
        t = x1; x1 = y1; y1 = t;
    }
    if( x0 > x1 ){
        t = x0; x0 = x1; x1 = t;
        t = y0; y0 = y1; y1 = t;
    }
    int16_t dx, dy;
    dx = x1 - x0;
    dy = abs(y1 - y0);
    int16_t err = dx / 2;
    int16_t ystep;
    if( y0 < y1 ){
        ystep = 1;
    }else{
        ystep = -1;
    }
    for( ; x0<=x1; x0++ ){
        if( steep ){
        	ssd1306SetPixel( y0, x0, color );
        }else{
        	ssd1306SetPixel( x0, y0, color );
        }
        err -= dy;
        if( err < 0 ){
            y0 += ystep;
            err += dx;
        }
    }
}
void IFA gfx_drawRect( int16_t x, int16_t y, int16_t w, int16_t h, uint16_t color ){
    
    gfx_drawLine( x, y, x+w-1, y, color );
    gfx_drawLine( x, y+h-1, x+w-1, y+h-1, color );
    gfx_drawLine( x, y, x, y+h-1, color );
    gfx_drawLine( x+w-1, y, x+w-1, y+h-1, color );
}
void IFA gfx_fillRect( int16_t x, int16_t y, int16_t w, int16_t h, uint16_t color ){
    int16_t i = 0;
    if( h > w ){
        for( i = x ; i < x+w ; i++ ){
            gfx_drawLine( i, y, i, y+h-1, color );
        }
    }else{
        for( i = y ; i < y+h ; i++ ){
            gfx_drawLine( x, i, x+w-1, i, color );
        }
    }
}
// circle outline
void IFA gfx_drawCircle( int16_t x0, int16_t y0, int16_t r,uint16_t color ){

    int16_t f = 1 - r;
    int16_t ddF_x = 1;
    int16_t ddF_y = -2 * r;
    int16_t x = 0;
    int16_t y = r;
    ssd1306SetPixel( x0 , y0+r, color );
    ssd1306SetPixel( x0 , y0-r, color );
    ssd1306SetPixel( x0+r, y0 , color );
    ssd1306SetPixel( x0-r, y0 , color );
    while( x < y ){
        if( f >= 0 ){
            y--;
            ddF_y += 2;
            f += ddF_y;
        }
        x++;
        ddF_x += 2;
        f += ddF_x;
        ssd1306SetPixel( x0 + x, y0 + y, color );
        ssd1306SetPixel( x0 - x, y0 + y, color );
        ssd1306SetPixel( x0 + x, y0 - y, color );
        ssd1306SetPixel( x0 - x, y0 - y, color );
        ssd1306SetPixel( x0 + y, y0 + x, color );
        ssd1306SetPixel( x0 - y, y0 + x, color );
        ssd1306SetPixel( x0 + y, y0 - x, color );
        ssd1306SetPixel( x0 - y, y0 - x, color );
    }
}

void IFA gfx_drawTriangle( int16_t x0, int16_t y0,int16_t x1, int16_t y1, int16_t x2, int16_t y2, uint16_t color ){
    
    gfx_drawLine( x0, y0, x1, y1, color );
    gfx_drawLine( x1, y1, x2, y2, color );
    gfx_drawLine( x2, y2, x0, y0, color );
}

void IFA gfx_GCLDFont(uint8_t font)
{
	currentFont=font;  // save for switching elsewhere
	switch (font)
	{
	case 1 : ch32=digital36x69; break;
	case 2 : ch32=digital17x32; break;
	case 3 : ch32=trebuchet; break;
	case 4 : ch32=calibri21x22; break;
	case 5 : ch32=calibri32x34; break;
	case 6 : ch32=petesfont24x28; break;
	case 7 : ch32=meteocons21x25; break;
	case 8 : ch32=ms_reference_sans_serif16x16; break;
	case 9 : ch32=Basic8x8; break;
	case 10: ch32=assorted1_19x21; break;
	case 11: ch32=assorted2_19x21; break;
	case 12: ch32=assorted3_19x21; break;
	case 0 :
	default: ch32=calibri16x17; break;
	}
   _width=read_rom_uint8(ch32++);
   _height=read_rom_uint8(ch32++);
   _offset=read_rom_uint8(ch32++);
	bytes_high=(_height+7)>>3; // rounded up bytes for a vertical slice of the char
   c_size=((_width*bytes_high)+1);

}



// This draws GCLD characters on the SSD1306
//uint8_t IFA draw_GCLDChar(uint16_t x, uint16_t y, uint8 _width, uint8 _height, uint32_t ch32, uint8_t mono, uint8_t ch){
void IFA ssd1306GCLDChar(uint8_t ch){
        uint8_t cwid;
	    uint32_t i;
	    uint32_t var_width;
        uint8_t t_height;
		uint16_t x;
		uint16_t y;

	    const uint8_t *ch32t;

	    ch32t=ch32;

	    //ch32t+=((ch-_offset)*((_width*bytes_high)+1));
	    ch32t+=((ch-_offset)*c_size);
	    cwid=read_rom_uint8(ch32t++);
        x=cursor_x; y=cursor_y;
	    if (mono) var_width=_width; else var_width=cwid; // this should speed up non-mono output - less columns

	    for ( i = 0; i < var_width; i++ ) {
                      uint32_t j;
                      uint16_t xi=x+i;
                      const uint8_t *ib=ch32t+(i*bytes_high);
                      t_height=_height; // only do ACTUALLY required height
                      for ( j = 0; j < bytes_high; j++ ) {
                      	   	  uint16_t jy=(j<<3)+y;
                              uint8_t dat = read_rom_uint8( ib + j );
                              uint8_t bit;
                              for (bit = 0; bit < 8; bit++) {
                                     if (t_height)
                                 	 {
                                     if (dat & (1<<bit))
                                     ssd1306SetPixel( x+i, y+(j<<3)+bit, 1 );
                                     else ssd1306SetPixel( x+i, y+(j<<3)+bit, 0 ); // don't really want that fill
                                     --t_height;
                                 	 }
                              }
                      }
              }
 cursor_x+=var_width;
}


static uint32_t thevalue;

static IFA char *parse( char *p)
		{
	    thevalue=0;
	    while ((*p>='0')&&(*p<='9')) { thevalue*=10; thevalue+=(*p)-'0'; p++; }
		if (*p==';') return ++p;
		if (*p==',') return ++p;
		return p;
		}


void IFA gfx_glyph(uint8_t wf, uint8_t x)
{
	 uint8_t tfont;
	 tfont=currentFont;
	 switch (wf)
	 {
	 case 1 :gfx_GCLDFont(10); break;
	 case 2 :gfx_GCLDFont(11); break;
	 case 3 :gfx_GCLDFont(12); break;
	 default :gfx_GCLDFont(10); break;
	 }
	 ssd1306GCLDChar(x);
	 gfx_GCLDFont(tfont);
}


void IFA ssd1306GCLDString (uint8_t *st)
{
	char tstore[12];
	while (*st)
			{
			if (*st=='\n')  { cursor_x=x_master; cursor_y+=_height; st++; continue;  }
			if (*st=='\r')  { st++; continue;  }
			switch(*st)
				{
		 	 	 case '$' : switch (*++st)
							 {
		 	 	 	 	 	 case '$' : break;
		 	 	 	 	 	 case 'S' : st=parse(++st);
		 	 	 	 	                          i2c_master_gpio_init();    // init i2c (RTC, OLED)
		 	 	 	 	 						  i2c_master_init();
		 	 	 	 	 						  if (thevalue) display_init_64(); else display_init_32();
		 	 	 	 	 						  display_clear();
		 	 	 	 	 						  display_on();
		 	 	 	 	 						  gfx_setCursor(0,0); gfx_GCLDFont(9);
		 	 	 	 	 						  continue;
		 	 	 	 	 	 case 'P' : st=parse(++st); int xx=thevalue; st=parse(st); int yy=thevalue; x_master=xx; cursor_x=xx; cursor_y=yy; continue;
		 	 	 	 	 	 case 'M' : st=parse(++st); if (thevalue==1) mono=1; else mono=0; continue;
		 	 	 	 	     case 'F' : st=parse(++st); if (thevalue<=12) gfx_GCLDFont(thevalue); continue;

		 	 	 	 	     case 'L' : st=parse(++st); xx=thevalue; st=parse(st); yy=thevalue; st=parse(st); int xx2=thevalue; st=parse(st); int yy2=thevalue;
		 	 	 	 	     	 	 	gfx_drawLine(xx,yy,xx2,yy2,1);continue;
		 	 	 	 	     case 'E' : st=parse(++st); xx=thevalue; st=parse(st); yy=thevalue; st=parse(st); xx2=thevalue; st=parse(st); yy2=thevalue;
		 	 	 	 	     	 	 	gfx_drawRect(xx,yy,xx2,yy2,1);continue;	 // 2nd param width and height
		 	 	 	 	     case 'I' : st=parse(++st); xx=thevalue; st=parse(st); yy=thevalue; st=parse(st); xx2=thevalue; st=parse(st); yy2=thevalue;
		 	 	 	 	     	 	 	gfx_fillRect(xx,yy,xx2,yy2,0); continue;	 // 2nd param width and height
		 	 	 	 	     case 'W' : display_clear(); ++st; continue;

		 	 	 	 	 	 case 'T' : ++st; os_sprintf(tstore,"%02d:%02d:%02d",tm.Hour, tm.Minute, tm.Second); ssd1306GCLDString(tstore); continue;
		 	 	 	 	 	 case 'D' : ++st; os_sprintf(tstore,"%02d:%02d:%02d",tm.Day, tm.Month, tm.Year); ssd1306GCLDString(tstore); continue;
		 	 	 	 	 	 case 'A' : ++st; struct ip_info theIp; wifi_get_ip_info(0, &theIp);
		 	 	 	 	 				os_sprintf(tstore, "%d:%d:%d:%d", theIp.ip.addr & 255, (theIp.ip.addr >> 8) & 255, (theIp.ip.addr >> 16) & 255, theIp.ip.addr >> 24);
		 	 	 	 	 			    ssd1306GCLDString(tstore); continue;
		 	 	 	 	     case 'G' : st=parse(++st); xx=thevalue; if ((xx<1) || (xx>3)) continue;
		 	 	 	 	 	 	 	 	if (*st==0) continue;
		 	 	 	 	 	 	 	 	gfx_glyph(xx,*st++);
		 	 	 	 	 	 	 	 	continue;
		 	 	 	 	     case 'V' : ++st; os_sprintf(tstore,"Ver: %s",SYSTEM_VER); ssd1306GCLDString(tstore); continue;
		 	 	 	 	     case 'R' : ++st; os_sprintf(tstore,"RSSI: %d",wifi_station_get_rssi()); ssd1306GCLDString(tstore); continue;
		 	 	 	 	     case 'U' : ++st; os_sprintf(tstore,"Up Time: %d:%02d:%02d \r\n",  secondsUp / (60 * 60), (secondsUp / 60) % 60, secondsUp % 60); ssd1306GCLDString(tstore); continue;
		 	 	 	 	     case 'N' : ++st; os_sprintf(tstore,"NAME: %s",sysCfg.base); ssd1306GCLDString(tstore); continue;
		 	 	 	 	     case 'O' : ++st; os_sprintf(tstore,"Volts: %d.%02d", (analog * 1000 / sysCfg.calibrate) / 100, (analog * 1000 / sysCfg.calibrate) % 100); ssd1306GCLDString(tstore); continue;



		 	 	 	 	     default: break;
						}
				}
			ssd1306GCLDChar(*st++);
			}
    display_update();
}

