/*
 * File       : ina219.h
 * Description: 
 * Author     : Davyd Norris
 *
 * Copyright(c) 2019 Olive Grove IT Pty Ltd
 */

#ifndef _INA219_H_
#define _INA219_H_
#include "aidan_and_petes.h"

void IFA ina219_begin();
int16_t IFA ina219_getBusVoltage_raw();
int16_t IFA ina219_getShuntVoltage_raw();
int16_t IFA ina219_getCurrent_raw();

#endif /* _INA219_H_ */
