/*
 * File       : matrix.h
 * Description: 
 * Author     : Davyd Norris
 *
 * Copyright(c) 2019 Olive Grove IT Pty Ltd
 */

#ifndef _MATRIX_H_
#define _MATRIX_H_

void IFA MLED_init(uint8_t intensity, uint8_t dataPin, uint8_t clockPin);
void IFA MLED_clear();
void IFA MLED_dot(uint8_t x, uint8_t y, uint8_t draw);
void IFA MLED_display();

#endif /* _MATRIX_H_ */
