/*
 * File       : ssd1351.h
 * Description: 
 * Author     : Davyd Norris
 *
 * Copyright(c) 2019 Olive Grove IT Pty Ltd
 */

#ifndef _SSD1351_H_
#define _SSD1351_H_

 void IFA writeCommand(uint8_t c);
 void IFA writeData(uint8_t c);
 void IFA ssd1351GCLDString(uint8_t* st);
 void IFA s6d02a1GCLDString(uint8_t* st);
#endif /* _SSD1351_H_ */
