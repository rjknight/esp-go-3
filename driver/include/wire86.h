/*
 * File       : wire86.h
 * Description: 
 * Author     : Davyd Norris
 *
 * Copyright(c) 2019 Olive Grove IT Pty Ltd
 */

#ifndef _WIRE86_H_
#define _WIRE86_H_
#include <c_types.h>

void ICACHE_FLASH_ATTR twi_init(void);
unsigned char ICACHE_FLASH_ATTR twi_writeTo(unsigned char address, unsigned char * buf, unsigned int len, unsigned char sendStop);
unsigned char ICACHE_FLASH_ATTR twi_readFrom(unsigned char address, unsigned char* buf, unsigned int len, unsigned char sendStop);
#endif /* _WIRE86_H_ */
