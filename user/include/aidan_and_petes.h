/*
 * aidan_and_petes.h
 *
 *  Created on: 16 Mar 2015
 *      Author: Aidan
 */

#ifndef USER_AIDAN_AND_PETES_H_
#define USER_AIDAN_AND_PETES_H_

#include "espmissingincludes.h"
#include "ets_sys.h"
#include "uart.h"
#include "osapi.h"
#include "mqtt.h"
#include "wifi.h"
#include "config.h"
#include "debug.h"
#include "gpio.h"
#include "user_interface.h"
#include "mem.h"
#include <stdarg.h>


#define USE_OPTIMIZE_PRINTF

// attempt to re-use string functions with different character drivers - very early steps..
//void  gfx_write( uint8_t); // this one from the ssd1306.c page.

// Aidans Additions

#include "iodefs.h"

#include "httpdespfs.h"
#include "cgi.h"
#include "cgiwifi.h"
#include "auth.h"

#ifndef IFA
#define IFA		ICACHE_FLASH_ATTR __attribute__((aligned(4)))
#endif

#ifndef IRA
#define IRA     ICACHE_RODATA_ATTR __attribute__((aligned(4)))
#endif

MQTT_Client mqttClient; // Main MQTT client

void iprintf(uint16_t debug_type, char *fmt, ... );

static uint8_t wsBitMask;
uint8_t indic_buf[3];
uint32_t secondsUp;
uint16_t analog, analogAccum;

typedef struct
{
	uint8_t Second;
	uint8_t Minute;
	uint8_t Hour;
	uint8_t Wday;     // day of week, Sunday is day 1
	uint8_t Day;
	uint8_t Month;
	uint16_t Year;
	unsigned long Valid;
} tm_t;

extern tm_t tm;

extern uint16_t spiActive;

uint8  read_rom_uint8(const uint8* addr);
#endif /* USER_AIDAN_AND_PETES_H_ */
