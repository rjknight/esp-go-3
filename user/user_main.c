/* main.c
 *
 * Copyright (c) 2014-2015, Aidan Ruff (aidan@ruffs.org) & Peter Scargill (pete@scargill.net)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * * Neither the name of Redis nor the names of its contributors may be used
 * to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "ets_sys.h"
#include "uart.h"
#include "osapi.h"
#include "mqtt.h"
#include "wifi.h"
#include "config.h"
#include "debug.h"
#include "gpio.h"
#include "user_interface.h"
#include "mem.h"


// Aidans Additions

#include "iodefs.h"

#include "httpdespfs.h"
#include "cgi.h"
#include "cgiwifi.h"
#include "auth.h"

#include "aidan_and_petes.h"
#include "petes.h"
#include "aidans.h"

//int enable_debug_messages = INFO | DEBUG | RESPONSE;
int enable_debug_messages = INFO | RESPONSE;

#ifndef SPI_FLASH_SIZE_MAP
#define SPI_FLASH_SIZE_MAP 4
#endif

#if ((SPI_FLASH_SIZE_MAP == 0) || (SPI_FLASH_SIZE_MAP == 1))
#define SYSTEM_PARTITION_OTA_SIZE							0x0
#define SYSTEM_PARTITION_OTA_2_ADDR							0x0
#define SYSTEM_PARTITION_RF_CAL_ADDR						0x0
#define SYSTEM_PARTITION_PHY_DATA_ADDR						0x0
#define SYSTEM_PARTITION_SYSTEM_PARAMETER_ADDR				0x0
#define SYSTEM_PARTITION_CUSTOMER_PRIV_PARAM_ADDR           0x0
#error "The flash map is not supported"
#elif (SPI_FLASH_SIZE_MAP == 2)
#define SYSTEM_PARTITION_OTA_SIZE							0x7A000
#define SYSTEM_PARTITION_OTA_2_ADDR							0x81000
#define SYSTEM_PARTITION_RF_CAL_ADDR						0xfb000
#define SYSTEM_PARTITION_PHY_DATA_ADDR						0xfc000
#define SYSTEM_PARTITION_SYSTEM_PARAMETER_ADDR				0xfd000
#define SYSTEM_PARTITION_CUSTOMER_PRIV_PARAM_ADDR           0x7c000
#elif (SPI_FLASH_SIZE_MAP == 3)
#define SYSTEM_PARTITION_OTA_SIZE							0x7A000
#define SYSTEM_PARTITION_OTA_2_ADDR							0x81000
#define SYSTEM_PARTITION_RF_CAL_ADDR						0x1fb000
#define SYSTEM_PARTITION_PHY_DATA_ADDR						0x1fc000
#define SYSTEM_PARTITION_SYSTEM_PARAMETER_ADDR				0x1fd000
#define SYSTEM_PARTITION_CUSTOMER_PRIV_PARAM_ADDR           0x7c000
#elif (SPI_FLASH_SIZE_MAP == 4)
#define SYSTEM_PARTITION_OTA_SIZE							0x7A000
#define SYSTEM_PARTITION_OTA_2_ADDR							0x81000
#define SYSTEM_PARTITION_RF_CAL_ADDR						0x3fb000
#define SYSTEM_PARTITION_PHY_DATA_ADDR						0x3fc000
#define SYSTEM_PARTITION_SYSTEM_PARAMETER_ADDR				0x3fd000
#define SYSTEM_PARTITION_CUSTOMER_PRIV_PARAM_ADDR           0x7c000
#elif (SPI_FLASH_SIZE_MAP == 5)
#define SYSTEM_PARTITION_OTA_SIZE							0xFA000
#define SYSTEM_PARTITION_OTA_2_ADDR							0x101000
#define SYSTEM_PARTITION_RF_CAL_ADDR						0x1fb000
#define SYSTEM_PARTITION_PHY_DATA_ADDR						0x1fc000
#define SYSTEM_PARTITION_SYSTEM_PARAMETER_ADDR				0x1fd000
#define SYSTEM_PARTITION_CUSTOMER_PRIV_PARAM_ADDR           0xfc000
#elif (SPI_FLASH_SIZE_MAP == 6)
#define SYSTEM_PARTITION_OTA_SIZE							0xFA000
#define SYSTEM_PARTITION_OTA_2_ADDR							0x101000
#define SYSTEM_PARTITION_RF_CAL_ADDR						0x3fb000
#define SYSTEM_PARTITION_PHY_DATA_ADDR						0x3fc000
#define SYSTEM_PARTITION_SYSTEM_PARAMETER_ADDR				0x3fd000
#define SYSTEM_PARTITION_CUSTOMER_PRIV_PARAM_ADDR           0xfc000
#else
#error "The flash map is not supported"
#endif

typedef enum UserPartition {
	RBOOT_BOOTLOADER = SYSTEM_PARTITION_CUSTOMER_BEGIN,
	SYSTEM_PARTITION_CUSTOMER_PRIV_PARAM
} UserPartition_t;


static const partition_item_t at_partition_table[] = {
{ RBOOT_BOOTLOADER, 					0x00000,									0x2000 },
{ SYSTEM_PARTITION_RF_CAL, 				SYSTEM_PARTITION_RF_CAL_ADDR, 				0x1000 },
{ SYSTEM_PARTITION_PHY_DATA, 			SYSTEM_PARTITION_PHY_DATA_ADDR,				0x1000 },
{ SYSTEM_PARTITION_SYSTEM_PARAMETER, 	SYSTEM_PARTITION_SYSTEM_PARAMETER_ADDR,		0x3000 },
{ SYSTEM_PARTITION_CUSTOMER_PRIV_PARAM, SYSTEM_PARTITION_CUSTOMER_PRIV_PARAM_ADDR, 	0x1000 },
};

//uint32 user_iram_memory_is_enabled(void)
//{
//	return 1;
//}

void ICACHE_FLASH_ATTR user_pre_init(void)
{
    if(!system_partition_table_regist(at_partition_table, sizeof(at_partition_table)/sizeof(at_partition_table[0]),SPI_FLASH_SIZE_MAP)) {
		os_printf("system_partition_table_regist fail\r\n");
		while(1);
	}
}

static void IFA wifi_station_scan_done(void *arg, STATUS status) {
	uint8 ssid[33];
	char tBuf[84];
	char dBuf[84];

	os_sprintf(tBuf, "%s/fromesp/scan", sysCfg.base);
	if (status == OK) {
		struct bss_info *bss_link = (struct bss_info *)arg;

		while (bss_link != NULL) {
			os_memset(ssid, 0, 33);
			if (os_strlen(bss_link->ssid) <= 32) {
				os_memcpy(ssid, bss_link->ssid, os_strlen(bss_link->ssid));
			} else {
				os_memcpy(ssid, bss_link->ssid, 32);
			}
			os_sprintf(dBuf, "WiFi Scan: (%d,\"%s\",%d)\r\n", bss_link->authmode, ssid, bss_link->rssi);
			iprintf(RESPONSE, dBuf);
			os_sprintf(dBuf, "{\"mode\":%d,\"ssid\":\"%s\",\"rssi\":%d}", bss_link->authmode, ssid, bss_link->rssi);
			MQTT_Publish(&mqttClient, tBuf, dBuf, os_strlen(dBuf), 0, 0);
			bss_link = bss_link->next.stqe_next;
		}
	} else {
		iprintf(RESPONSE, "WIFI scan failed %d\n", status);
	}
}

void ICACHE_FLASH_ATTR user_init(void)
{
	system_set_os_print(0);
	petes_initialisation(); // Sets up the ports and initialises various values needed by the MQTT call backs, time etc
	setupwebpage_init(); // Set up the configuration/control web pages - or not
}

void ICACHE_FLASH_ATTR user_spi_flash_dio_to_qio_pre_init(void) {}
